/**
 * This environment used when front-end bundles as a part of back-end resources.
 * serviseURL must be empty string.
 */
export const environment = {
  serverURL: '',
  production: true
};
