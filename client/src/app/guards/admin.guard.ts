import {EventEmitter, Injectable} from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router} from '@angular/router';
import { Observable } from 'rxjs';
import {AuthService} from "../services/auth.service";

@Injectable({
  providedIn: 'root'
})
export class AdminGuard implements CanActivate {

  constructor(private authService: AuthService, private router: Router) {
  }


  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot) {

    console.log(this.authService.auth);
    if(this.authService.hasRole('ROLE_ANONYMOUS')){
      console.log(` --- goto login`);
      this.router.navigate(['login']);
      return false;
    }

    if(this.authService.hasRole('ROLE_ADMIN')){
      return true;
    }

    return false;
  }
  
}
