import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'categoryStatus'
})
export class CategoryStatusPipe implements PipeTransform {

  transform(value: any, ...args: any[]): any {
    let v = Number(value);
    switch (v) {
      case 0:
        return "Closed";
      case 1:
        return "Enabled";
      case 2:
        return "New";
      default:
        return "???";
    }

  }

}
