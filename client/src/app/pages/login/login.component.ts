import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {AuthService} from "../../services/auth.service";
import {Authentication} from "../../model/authentication";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  username: string = 'adm';
  password : string = 'pass';
  errorMessage = 'Invalid Credentials';
  successMessage: string;
  invalidLogin = false;
  loginSuccess = false;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private authenticationService: AuthService) { }

  ngOnInit() {
  }

  handleLogin1() {
    this.authenticationService.authenticationService(this.username, this.password).subscribe(()=> {
    }, err => {
      console.error(` --- Error login: ${err}`);
      console.error(err);
      this.invalidLogin = true;
      this.loginSuccess = false;
    },() => {
      this.invalidLogin = false;
      this.loginSuccess = true;
      this.successMessage = 'Login Successful.';
      this.router.navigate(['/']);
    });
  }


  handleLogin(){
    this.authenticationService.getAuthenticationResponse(this.username, this.password)
    // resp is of type `HttpResponse<Config>`
      .subscribe(resp => {
        // display its headers
        console.log(resp);
        console.log(resp.headers);
        const keys = resp.headers.keys();
        let headers = keys.map(key =>
          `${key}: ${resp.headers.get(key)}`);
        headers.forEach(h =>  console.log(`${h}`));
        keys.forEach( k => console.log(`${k} -> ${resp.headers.get(k)}`));
        console.log(resp.body);

        this.authenticationService.auth = <Authentication>resp.body;
      }, err => {
        console.error(` --- Error login: ${err}`);
        console.error(err);
        this.invalidLogin = true;
        this.loginSuccess = false;
      }, ()  => {
        this.invalidLogin = false;
        this.loginSuccess = true;
        this.successMessage = 'Login Successful.';
        this.authenticationService.registerSuccessfulLogin(this.username, this.password);
        this.router.navigate(['/']);
      });
  }


}
