import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {AuthService} from "../../services/auth.service";
import {Router} from "@angular/router";
import {ModelService} from "../../services/model.service";
import {Category} from "../../model/category";
import {Product} from "../../model/product";

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit {

  strSearch: string = "";


  constructor(public authService: AuthService,
              private router: Router, public modelService: ModelService) {
    // console.log(` --- constructor IndexComponent`);
  }

  ngOnInit() {
    // console.log(` --- ngOnInit index component`);
  }

  logout() {
    this.authService.logout();
    this.router.navigateByUrl('/');
  }

  get categories(): Category[] {
    const pageIndex = (this.modelService.selectedCategoryPage - 1) * this.modelService.categoriesPerPage;
    return this.modelService.getCategories()
      .slice(pageIndex, pageIndex + this.modelService.categoriesPerPage);
  }

  changePage(newPage: number) {
    this.modelService.selectedProductPage = 1;
    this.modelService.selectedCategoryPage = newPage;
  }

  changeCategoryId(categoryId: number) {
    // console.log(` --- changeCategoryId ${categoryId}`);
    this.strSearch = "";
    this.modelService.selectedProductPage = 1;
    this.modelService.currentCategoryId = categoryId;
    this.modelService.refreshProducts();
  }


  get pageNumbers(): number[] {
    return Array(Math.ceil(this.modelService
      .getCategories().length / this.modelService.categoriesPerPage))
      .fill(0).map((x, i) => i + 1);
  }


  get products(): Product[] {
    return this.modelService.getProducts();
    // const pageIndex = (this.selectedPage - 1) * this.categoriesPerPage;
    // return this.modelService.getCategories()
    //   .slice(pageIndex, pageIndex + this.categoriesPerPage);
  }

  changePageSize(newSize: number) {
    this.modelService.productPerPage = Number(newSize);
    this.changeProductPage(1);
  }

  changeProductPage(newPage: number) {
    this.modelService.selectedProductPage = newPage;
    this.modelService.getProductCategoryPage(this.modelService.currentCategoryId,
      (this.modelService.selectedProductPage - 1), this.modelService.productPerPage, this.strSearch);

  }


  get buttonsProductPage(): number[] {
    let n = Math.ceil(this.modelService
      .countProduct / this.modelService.productPerPage);

    let r: number[];
    // r = Array.apply(null, {length: n}).map(Number.call, Number).map(n => ++n);

    if (n < 7) {
      r = Array.apply(null, {length: n}).map(Number.call, Number).map(n => ++n);
      return r;
    }

    if (this.modelService.selectedProductPage <= 5) {
      r = Array(Math.ceil(5))
        .fill(0).map((x, i) => i + 1);
      if (n != NaN) {
        r.push(n);
      }
    }

    if (this.modelService.selectedProductPage > 3 && this.modelService.selectedProductPage < n - 3) {
      r = Array(Math.ceil(5))
        .fill(this.modelService.selectedProductPage - 3).map((x, i) => i + 1 + x);
      r.push(n);
      r.unshift(1);
    }

    if (this.modelService.selectedProductPage >= n - 3) {
      r = Array(Math.ceil(5))
        .fill(n - 5).map((x, i) => i + 1 + x);
      r.unshift(1);
    }

    return r;
  }

  private timerId;
  search(str: string) {
    this.strSearch = str;
    if (this.timerId) {
      console.log(`delete timer  ${this.timerId}`);
      clearTimeout(this.timerId);
    }
    this.modelService.selectedProductPage = 1;
    let self = this;
    this.timerId = setTimeout(getSearchProducts, 500, self);
    console.log(this.timerId);

    function getSearchProducts(t: IndexComponent){
      t.modelService.getProductCategoryPage(t.modelService.currentCategoryId, (t.modelService.selectedProductPage - 1), t.modelService.productPerPage, t.strSearch);
      // alert(c.strSearch);
    }
  }

  cleanStrSearch(){
    this.strSearch = "";
    this.modelService.selectedProductPage = 1;
    this.modelService.getProductCategoryPage(this.modelService.currentCategoryId, (this.modelService.selectedProductPage - 1), this.modelService.productPerPage, this.strSearch);
  }

  printCardProduct(id: number){

    let protocol = window.location.protocol;
    let hostname = window.location.hostname;
    // let port = window.location.port;
    let port = "8080";

    let url = `${protocol}//${hostname}:${port}/print-product?id=${id}`;
    console.log(url);

    window.open(url, "_blank");
  }
}
