import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { IndexComponent } from './pages/index/index.component';
import {RouterModule, Routes} from "@angular/router";
import {ContentsComponent} from "./pages/contents/contents.component";
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import { ErrorMessageComponent } from './components/error-message/error-message.component';
import { LoginComponent } from './pages/login/login.component';
import {FormsModule} from "@angular/forms";
import {HttpInterceptorService} from "./interceptors/http-interceptor.service";
import { ErrorPageComponent } from './pages/error-page/error-page.component';
import {AdminGuard} from "./guards/admin.guard";


const routes: Routes = [
  {
    path: '',
    component: IndexComponent
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'errorLogin',
    component: ErrorPageComponent
  },

  // { path: '', loadChildren: './admin/admin.module#AdminModule'},
  {
    path: 'contents',
    component: ContentsComponent,
  },
  { path: 'admin', loadChildren: './admin/admin.module#AdminModule', canActivate: [AdminGuard]},
  { path: '**', redirectTo: '/' }
]

@NgModule({
  declarations: [
    AppComponent,
    IndexComponent,
    ContentsComponent,
    ErrorMessageComponent,
    LoginComponent,
    ErrorPageComponent
  ],
  imports: [
    BrowserModule, RouterModule.forRoot(routes), HttpClientModule, FormsModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpInterceptorService,
      multi: true
    },
    AdminGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
