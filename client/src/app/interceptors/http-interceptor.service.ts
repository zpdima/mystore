import { Injectable } from '@angular/core';
import {HttpEvent, HttpHandler, HttpHeaders, HttpInterceptor, HttpRequest} from "@angular/common/http";
import {AuthService} from "../services/auth.service";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class HttpInterceptorService implements HttpInterceptor {


  constructor(private authenticationService: AuthService) {}


  // intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
  //   console.log(` --- HttpInterceptorService`);
  //   console.log(req);
  //   if (this.authenticationService.isUserLoggedIn() && req.url.indexOf('basicauth') === -1) {
  //     const authReq = req.clone({
  //       headers: new HttpHeaders({
  //         'Content-Type': 'application/json',
  //         'Authorization': `Basic ${window.btoa(this.authenticationService.username + ":" + this.authenticationService.password)}`
  //         // 'withCredentials': 'true',
  //         // 'X-Requested-With': 'XMLHttpRequest',
  //         // 'Cookie': 'JSESSIONID=4DD03B99E992A05AC1859B1482E5B04F'
  //       })
  //     });
  //     console.log(authReq);
  //     return next.handle(authReq);
  //   } else {
  //     console.log(req);
  //     return next.handle(req);
  //   }
  // }


  intercept(request:HttpRequest<any>, next:HttpHandler):Observable<HttpEvent<any>> {
    const clonedRequest =
      request.clone(
        {withCredentials: true,
          setHeaders:{'Accept': 'application/json;charset=UTF-8'}}
      );

    return next.handle(clonedRequest);
  }
}
