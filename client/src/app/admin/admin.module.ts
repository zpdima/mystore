import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthComponent } from './pages/auth/auth.component';
import { AdminComponent } from './pages/admin/admin.component';
import {RouterModule} from "@angular/router";
import {AuthGuard} from "./auth.guard";
import { CategoryTableComponent } from './pages/category-table/category-table.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import { ProductTableComponent } from './pages/product-table/product-table.component';
import { CategoryEditorComponent } from './pages/category-editor/category-editor.component';
import { AdmErrorMessageComponent } from './pages/adm-error-message/adm-error-message.component';
import {CategoryStatusPipe} from "../pipes/category-status.pipe";
import { ProductEditorComponent } from './pages/product-editor/product-editor.component';


const routing = RouterModule.forChild([
  { path: 'auth', component: AuthComponent },
  { path: 'main', component: AdminComponent, canActivate: [AuthGuard],
    children: [
      { path: 'categories/:mode/:id', component: CategoryEditorComponent },
      { path: 'categories/:mode', component: CategoryEditorComponent },
      { path: 'categories', component: CategoryTableComponent },
      { path: 'products/:mode/:id', component: ProductEditorComponent },
      { path: 'products/:mode', component: ProductEditorComponent },
      { path: 'products', component: ProductTableComponent },
      { path: '**', redirectTo: 'products' }
    ]},
  { path: '**', redirectTo: 'main' }
]);


@NgModule({
  declarations: [AuthComponent, AdminComponent, CategoryTableComponent, ProductTableComponent, CategoryEditorComponent, AdmErrorMessageComponent, CategoryStatusPipe, ProductEditorComponent],
  imports: [
    CommonModule, FormsModule, routing, ReactiveFormsModule
  ]
})
export class AdminModule { }
