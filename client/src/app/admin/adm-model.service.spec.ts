import { TestBed } from '@angular/core/testing';

import { AdmModelService } from './adm-model.service';

describe('AdmModelService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AdmModelService = TestBed.get(AdmModelService);
    expect(service).toBeTruthy();
  });
});
