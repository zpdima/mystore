import { Component, OnInit } from '@angular/core';
import {AdmModelService} from "../../adm-model.service";
import {Router} from "@angular/router";
import {Category} from "../../../model/category";

@Component({
  selector: 'app-category-table',
  templateUrl: './category-table.component.html',
  styleUrls: ['./category-table.component.css']
})
export class CategoryTableComponent implements OnInit {

  public categoriesPerPage: number = 25
  public selectedPage: number = 1;
  strFind: string;

  currentId: number = null;

  constructor(public admModelService: AdmModelService,
              private router: Router) { }

  ngOnInit() {
  //console.log(` --- CategoryTableComponent ngOnInit()`);
    // this.admModelService.getCategories().forEach(c => {
    //   console.log(` --- category name = ${c.name}`);
    // });
  }

  getCategories(){
    return this.admModelService.getCategories();
  }

  deleteCategory(id: number){
  //console.log(` --- del id = ${id}`);
    this.admModelService.deleteCategoryById(id);

  }

  edit(id: number){
    let url = "/main/categories/edit/" + id;
  //console.log(` --- edit id = ${id} '${url}'`);
    this.router.navigateByUrl(url);
  }

  changePageSize(newSize: number) {
    this.categoriesPerPage = Number(newSize);
    this.changePage(1);
  }

  changePage(newPage: number) {
    this.selectedPage = newPage;
  }

  get buttonsPage(): number[] {
    let n = Math.ceil(this.admModelService
      .getCategories().length / this.categoriesPerPage);
    let r: number[] =  Array.apply(null, {length: n}).map(Number.call, Number).map(n => ++n);
    return r;
  }

  get categories(): Category[] {
    const pageIndex = (this.selectedPage - 1) * this.categoriesPerPage;
    return this.admModelService.getCategories()
      .slice(pageIndex, pageIndex + this.categoriesPerPage);
  }

  find(): Category[] {
    // console.log(` --- find ${this.strFind}`);
    if(this.strFind !== undefined && this.strFind.length > 0){
      return this.admModelService.getCategories()
        .filter(c => c.name.includes(this.strFind));
    }else{
      return this.categories;
    }
  }

}
