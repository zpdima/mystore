import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {AuthService} from "../../../services/auth.service";
import {AdmModelService} from "../../adm-model.service";

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {

  constructor(public authService: AuthService,
              private router: Router,
              private admModelService: AdmModelService) { }

  ngOnInit() {
  }

  logout() {
    this.admModelService.clear();
    this.authService.logout();
    this.router.navigateByUrl('/');
  }
}
