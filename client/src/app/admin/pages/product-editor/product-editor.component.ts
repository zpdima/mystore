import {Component, OnInit} from '@angular/core';
import {Product} from "../../../model/product";
import {AdmModelService} from "../../adm-model.service";
import {ActivatedRoute, Router} from "@angular/router";
import {ProductFormGroup} from "./form-model";

@Component({
  selector: 'app-product-editor',
  templateUrl: './product-editor.component.html',
  styleUrls: ['./product-editor.component.css']
})
export class ProductEditorComponent implements OnInit {

  editing = false;
  product: Product = new Product();

  formSubmitted: boolean = false;

  constructor(private admModelService: AdmModelService,
              private router: Router,
              activeRoute: ActivatedRoute) {
    this.editing = activeRoute.snapshot.params.mode == 'edit';
    if (this.editing) {
      Object.assign(this.product,
        admModelService.getProductById(activeRoute.snapshot.params.id));
      //console.log(this.product);
    }else{
      //console.log(` ---- new product`);
      this.product.name="new product";
      this.product.description="new product";
      this.product.categoryId=activeRoute.snapshot.params.id;
    }

  }

  ngOnInit() {
  }

  get jsonCategory() {
    return JSON.stringify(this.product);
  }


  form: ProductFormGroup = new ProductFormGroup();

  submitForm(form: any) {
    //console.log(` --- save ${this.category}`);
    this.formSubmitted = true;
    if (form.valid) {
      // this.addCategory(this.newCategory);
      // this.newCategory = new Category();
      // form.reset();
      this.admModelService.saveProduct(this.product);
      this.formSubmitted = false;
    }
  }


}
