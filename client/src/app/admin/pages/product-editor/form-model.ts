import {FormControl, FormGroup, Validators} from "@angular/forms";
import {CategoryFormControl} from "../category-editor/form-model";


export class ProductFormControl extends FormControl{

  label: string;
  modelProperty: string;
  type: string;

  constructor(label: string, property: string, value: any, validator: any, type?: string) {
    super(value, validator);
    this.label = label;
    this.modelProperty = property;
    this.type = type || "input"
  }

  getValidationMessages() {
    let messages: string[] = [];
    if (this.errors) {
      for (let errorName in this.errors) {
        switch (errorName) {
          case 'required':
            messages.push(`You must enter a ${this.label}`);
            break;
          case 'minlength':
            messages.push(`A ${this.label} must be at least ${this.errors['minlength'].requiredLength} characters`);
            break;
          case 'maxlength':
            messages.push(`A ${this.label} must be no more than ${this.errors['maxlength'].requiredLength} characters`);
            break;
          case 'pattern':
            messages.push(`The ${this.label} contains illegal characters`);
            break;
        }
      }
    }
    return messages;
  }
}

export class ProductFormGroup extends FormGroup {

  constructor() {
    super({
      name: new ProductFormControl('Name', 'name', '', Validators.compose([Validators.required,
        // Validators.pattern('^[A-Za-z0-9.\- ]+$'),
        Validators.minLength(3),
        Validators.maxLength(60)])),

      description: new ProductFormControl('Descripton', 'description', '',
        Validators.compose([Validators.required,
          // Validators.pattern('^[A-Za-z0-9.,; ]+$'),
          Validators.minLength(3),
          Validators.maxLength(60)])),

      remain: new ProductFormControl('Remain', 'remain', '',
        Validators.compose([Validators.required,
          Validators.pattern('^[0-9]+$')])),

      price: new ProductFormControl('Price', 'price', '',
      Validators.compose([Validators.required,
        Validators.pattern('^[0-9.]+$')])),

      categoryId: new ProductFormControl('categoryId', 'categoryId', '',
        Validators.compose([Validators.required]))


      // status: new ProductFormControl('Status', 'status', '',
      //   Validators.compose([Validators.required])
      //   // Validators.pattern('^[0-1]+$')])
      //   , "select")
    });
  }

  get productControls(): ProductFormControl[] {
    let controls: ProductFormControl[] = Object.keys(this.controls)
      .map(k => this.controls[k] as ProductFormControl);
    // controls.forEach(c => {console.log(` --- ${c.type}`)});

    return Object.keys(this.controls)
      .map(k => this.controls[k] as ProductFormControl).filter(c => c.type == "input");
  }

  getFormValidationMessages(form: any): string[] {
    let messages: string[] = [];
    this.productControls.forEach(c => c.getValidationMessages()
      .forEach(m => messages.push(m)));
    return messages;
  }


}


export class FormModel{

}
