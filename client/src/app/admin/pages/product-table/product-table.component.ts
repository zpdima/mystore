import {Component, OnInit} from '@angular/core';
import {AdmModelService} from "../../adm-model.service";
import {Router} from "@angular/router";
import {Category} from "../../../model/category";
import {Product} from "../../../model/product";

@Component({
  selector: 'app-product-table',
  templateUrl: './product-table.component.html',
  styleUrls: ['./product-table.component.css']
})
export class ProductTableComponent implements OnInit {

  public productsPerPage: number = 5;
  public selectedPage: number = 1;
  strSearch: string = "";

  currentId: number = null;


  constructor(public admModelService: AdmModelService,
              private router: Router) {
  }

  ngOnInit() {
  }


  changeCategory(catagoryId: number) {
    this.admModelService.currentCategoryId = catagoryId;
    this.admModelService.getProductCategoryPage(catagoryId, 0, this.productsPerPage, this.strSearch)
    console.log(` --- selected category id ${this.admModelService.currentCategoryId}`);
  }

  changePageSize(newSize: number) {
    this.productsPerPage = Number(newSize);
    this.changePage(1);
  }

  changePage(newPage: number) {
    this.selectedPage = newPage;
    // if (this.strSearch == "") {
    //   this.admModelService.getProductCategoryPage(this.admModelService.currentCategoryId, (this.selectedPage - 1), this.productsPerPage)
    // } else {
      this.admModelService.getProductCategoryPage(this.admModelService.currentCategoryId, (this.selectedPage - 1), this.productsPerPage, this.strSearch);
    // }

  }

  get buttonsPage(): number[] {
    let n = Math.ceil(this.admModelService
      .countProduct / this.productsPerPage);

    let r: number[];
    // r = Array.apply(null, {length: n}).map(Number.call, Number).map(n => ++n);

    if (n < 7) {
      r = Array.apply(null, {length: n}).map(Number.call, Number).map(n => ++n);
      return r;
    }

    if (this.selectedPage <= 5) {
      r = Array(Math.ceil(5))
        .fill(0).map((x, i) => i + 1);
      if (n != NaN) {
        r.push(n);
      }
    }

    if (this.selectedPage > 3 && this.selectedPage < n - 3) {
      r = Array(Math.ceil(5))
        .fill(this.selectedPage - 3).map((x, i) => i + 1 + x);
      r.push(n);
      r.unshift(1);
    }

    if (this.selectedPage >= n - 3) {
      r = Array(Math.ceil(5))
        .fill(n - 5).map((x, i) => i + 1 + x);
      r.unshift(1);
    }

    return r;
  }


  get pageNumbers(): number[] {
    return Array(Math.ceil(this.admModelService
      .getCategories().length / this.productsPerPage))
      .fill(0).map((x, i) => i + 1);
  }

  // find(): Product[] {
  //   // console.log(` --- find ${this.strSearch}`);
  //   if (this.strSearch !== undefined && this.strSearch.length > 0) {
  //     return this.admModelService.getProducts()
  //       .filter(p => p.name.includes(this.strSearch));
  //   } else {
  //     return this.admModelService.getProducts();
  //   }
  // }

  private timerId;

  search(str: string) {
    this.strSearch = str;
    if (this.timerId) {
      clearTimeout(this.timerId);
    }
    this.selectedPage = 1;
    let self = this;
    this.timerId = setTimeout(getSearchProducts, 500, self);

    function getSearchProducts(c: ProductTableComponent){
      c.admModelService.getProductCategoryPage(c.admModelService.currentCategoryId, (c.selectedPage - 1), c.productsPerPage, c.strSearch);
      // alert(c.strSearch);
    }
  }

  cleanStrSearch(){
    this.strSearch = "";
    this.selectedPage = 1;
    this.admModelService.getProductCategoryPage(this.admModelService.currentCategoryId, (this.selectedPage - 1), this.productsPerPage, this.strSearch);
  }

  deleteProduct(id: number){
    console.log(` --- del id = ${id}`);
    this.admModelService.deleteProductById(id);
  }

}
