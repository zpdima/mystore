import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {NgForm} from "@angular/forms";
import {AuthService} from "../../../services/auth.service";

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})
export class AuthComponent implements OnInit {
  public username: string;
  public password: string;
  public errorMessage: string;

  constructor(private router: Router,
              private auth: AuthService) {
  }

  ngOnInit(): void {
  }

  authenticate(form: NgForm) {
    if (form.valid) {
      this.router.navigateByUrl('/main');
    } else {
      this.errorMessage = 'Form Data Invalid';
    }
  }
}
