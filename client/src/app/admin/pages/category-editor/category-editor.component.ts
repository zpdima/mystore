import {Component, OnInit} from '@angular/core';
import {Category, Status} from "../../../model/category";
import {ActivatedRoute, Router} from "@angular/router";
import {AdmModelService} from "../../adm-model.service";
import {CategoryFormGroup} from "./form-model";

@Component({
  selector: 'app-category-editor',
  templateUrl: './category-editor.component.html',
  styleUrls: ['./category-editor.component.css']
})
export class CategoryEditorComponent implements OnInit {

  editing = false;
  category: Category = new Category();

  constructor(private admModelService: AdmModelService,
              private router: Router,
              activeRoute: ActivatedRoute) {
  //console.log(` --- ${activeRoute.snapshot.params.mode} ${activeRoute.snapshot.params.id} `);
    this.editing = activeRoute.snapshot.params.mode == 'edit';
    if (this.editing) {
      Object.assign(this.category,
        admModelService.getCategoryById(activeRoute.snapshot.params.id));
    //console.log(this.category);
    }else{
    //console.log(` ---- new category`);
      this.category.name="new category";
      this.category.description="new category";
      this.category.status=Status.NEW;

    }
  }

  ngOnInit() {
    // this.category.name="new category";
    // this.category.description="new category";
    // this.category.status=0;

  }

  get jsonCategory() {
    return JSON.stringify(this.category);
  }


  form: CategoryFormGroup = new CategoryFormGroup();

  getCategory(key: number): Category {
    return this.admModelService.getCategoryById(key);
  }

  // getCategories(): Category[] {
  //   return this.admModelService.getCategories();
  // }

  // addCategory(c: Category) {
  // //console.log('New Category: ' + this.jsonCategory);
  // }

  formSubmitted: boolean = false;


  submitForm(form: any) {
  //console.log(` --- save ${this.category}`);
    this.formSubmitted = true;
    if (form.valid) {
      // this.addCategory(this.newCategory);
      // this.newCategory = new Category();
      // form.reset();
      this.admModelService.saveCategory(this.category);
      this.formSubmitted = false;
    }
  }

  keysStatus() : Array<string> {
    var keys = Object.keys(Status);
    let r =  keys.slice(keys.length / 2);
    // console.log(r);
    return r;
  }



}
