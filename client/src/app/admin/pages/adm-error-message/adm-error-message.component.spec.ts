import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdmErrorMessageComponent } from './adm-error-message.component';

describe('AdmErrorMessageComponent', () => {
  let component: AdmErrorMessageComponent;
  let fixture: ComponentFixture<AdmErrorMessageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdmErrorMessageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdmErrorMessageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
