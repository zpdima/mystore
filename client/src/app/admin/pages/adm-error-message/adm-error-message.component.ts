import { Component, OnInit } from '@angular/core';
import {AdmModelService} from "../../adm-model.service";

@Component({
  selector: 'app-adm-error-message',
  templateUrl: './adm-error-message.component.html',
  styleUrls: ['./adm-error-message.component.css']
})
export class AdmErrorMessageComponent implements OnInit {

  constructor(public admModelService: AdmModelService) { }

  ngOnInit() {
  }

}
