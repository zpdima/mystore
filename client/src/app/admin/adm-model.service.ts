import {Injectable} from '@angular/core';
import {Category} from "../model/category";
import {CategoryService} from "../services/category.service";
import {ErrorMessageService} from "../services/error-message.service";
import {Router} from "@angular/router";
import {ErrorJson, IErrorJson} from "../model/error-json";
import {Product} from "../model/product";
import {ProductService} from "../services/product.service";

@Injectable({
  providedIn: 'root'
})
export class AdmModelService {

  showError: boolean = false;
  textError: string;
  jsonError: ErrorJson;


  public categories: Category[] = new Array<Category>();
  private locator = (c: Category, id: number) => c.id == id;
  public currentCategoryId: number;

  private products: Product[] = new Array<Product>();
  public countProduct: number;


  constructor(private categoryService: CategoryService,
              private productService: ProductService,
              private router: Router) {
    //console.log(` --- constructor AdmModelService`);
    this.categoryService.getCategories().subscribe(data => {
        this.categories = data
      }, err => {
        console.error(err);
        return err;
      },
      () => {
        //console.log("Ok");
      });
  }


  refreshCategories() {
    this.categoryService.getCategories().subscribe(data => {
        this.categories = data
      }, err => {
        console.error(err);
        return err;
      },
      () => {
        //console.log("Ok");
      });
  }

  getCategories(): Category[] {
    return this.categories;
  }

  getProducts(): Product[] {
    return this.products;
  }

  getCategoryById(id: number): Category {
    return this.categories.find(c => this.locator(c, id));
  }

  getProductById(id: number): Product {
    return this.products.find(p => p.id == id);
  }


  deleteCategoryById(id: number) {

    this.categoryService.deleteById(id).subscribe(data => {

      }, err => {
        console.error(err);
        if (err['error'] !== undefined) {
          this.textError = err['error'];
        } else {
          this.textError = "runtime error\n";
        }
        this.showError = true;

      },
      () => {
        //console.log("Ok");
        const index = this.categories.indexOf(this.getCategoryById(id), 0);
        //console.log(` --- index ${index}`);
        if (index > -1) {
          this.categories.splice(index, 1);
        }

      });
  }

  saveCategory(category: Category) {

    this.categoryService.saveCategory(category).subscribe(data => {
      }, err => {
        // console.error(err);

        // if(err instanceof IEntryJSON)
        if (err['error'] !== undefined && err['error']['errors'] !== undefined) { // err['error'] instanceof ErrorJson){
          // console.log(` --- !!! `);
          let e = err['error'] as ErrorJson;
          // console.log(` --- !!! ${e.status} ${e.message}`);
          this.jsonError = e;
          // e.errors.forEach(errorDB =>{
          //   console.log(` --- -- ${errorDB.defaultMessage} ${errorDB.objectName} ${errorDB.field} ${errorDB.rejectedValue}`);
          // });
        } else {
          this.textError = "runtime error\n";
        }
        // this.showError = true;

      },
      () => {
        //console.log("Ok");
        this.refreshCategories();
        this.jsonError = null;
        let url = "/admin/main/categories";
        this.router.navigateByUrl(url);

      });
  }

  getErrorByName(name: string) {
    if (this.jsonError && this.jsonError.errors) {
      return this.jsonError.errors.filter(e => e.field == name);
    } else {
      return null;
    }
  }

  resetErrorByName(name: string) {

    if (!this.jsonError) {
      return;
    }

    let count = this.jsonError.errors.length;
    for (let i = 0; i < count; i++) {
      let index = this.jsonError.errors.indexOf(this.jsonError.errors.find(e => e.field == name), 0);
      // console.log(` --- index ${index}`);
      if (index > -1) {
        this.jsonError.errors.splice(index, 1);
      }
    }

    // this.jsonError.errors = null;
  }

  getProductCategoryPage(categoryId: number, page: number, size: number, search?: string) {
    console.log(` --- find ${search}`);
    if (search) {
      console.log(` --- find ${search}`);
      this.productService.getCountProducts(categoryId, search).subscribe(data => {
          this.countProduct = data;
        }, err => {
          console.error(err);
          return err;

        }, () => {
          console.log(` --- count product ${this.countProduct}`)
          this.productService.getProductCategoryPage(categoryId, page, size, search).subscribe(data => {
              this.products = data
            }, err => {
              console.error(err);
              return err;
            },
            () => {
              //console.log("Ok");
            });
        }
      );

    } else {
      console.log(` --- all `);
      this.productService.getCountProducts(categoryId).subscribe(data => {
          this.countProduct = data;
        }, err => {
          console.error(err);
          return err;

        }, () => {
          console.log(` --- count product ${this.countProduct}`)
          this.productService.getProductCategoryPage(categoryId, page, size).subscribe(data => {
              this.products = data
            }, err => {
              console.error(err);
              return err;
            },
            () => {
              //console.log("Ok");
            });
        }
      );
    }


  }

  deleteProductById(id: number) {

    this.productService.deleteById(id).subscribe(data => {

      }, err => {
        console.error(err);
        if (err['error'] !== undefined) {
          this.textError = err['error'];
        } else {
          this.textError = "runtime error\n";
        }
        this.showError = true;

      },
      () => {
        // console.log("Ok");
        const index = this.products.findIndex(p => p.id == id);
        // console.log(` --- index ${index}`);
        if (index > -1) {
          this.products.splice(index, 1);
        }

      });
  }

  saveProduct(product: Product) {

    let saveProduct: Product;
    this.productService.saveProduct(product).subscribe(data => {
        saveProduct = <Product>data;
      }, err => {
        // console.error(err);
        if (err['error'] !== undefined && err['error']['errors'] !== undefined) { // err['error'] instanceof ErrorJson){
          let e = err['error'] as ErrorJson;
          this.jsonError = e;
        } else {
          this.textError = "runtime error\n";
        }
      },
      () => {
        //console.log("Ok");
        // this.refreshProducts();
        if(product.id) {
          this.products.splice(this.products.findIndex(p => p.id === product.id), 1, product);
        }else{
          this.products.push(saveProduct);
        }
        this.jsonError = null;
        let url = "/admin/main/products";
        this.router.navigateByUrl(url);

      });
  }


  clear() {
    this.categories = new Array<Category>();
    this.products = new Array<Product>();
  }

}
