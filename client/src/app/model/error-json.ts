export interface IError {
  codes?: string[];
  arguments?: any[];
  defaultMessage?: string;
  objectName?: string;
  field?: string;
  rejectedValue?: string;
  bindingFailure?: boolean;
  code?: string;
}

export interface IErrorJson{
  timestamp?: string;
  status?: number;
  error?: string;
  errors?: IError[];
  message?: string;
  path?: string;
}

export class ErrorJson {
  timestamp?: string;
  status?: number;
  error?: string;
  errors?: IError[];
  message?: string;
  path?: string;
}
