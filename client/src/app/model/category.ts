
export enum Status {
  CLOSED,
  OPENED,
  NEW
}


export interface ICategory {
  // title: string;
  id?: number;
  name?: string;
  description?: string;
  status?: Status;
  testField?: bigint;
}

export class Category implements ICategory{

  id?: number;
  name: string;
  description?: string;
  status: Status;
  testField?: bigint;

  constructor(id?: number, name?: string, description?: string, status?: Status, testField?: bigint) {
  }

}
