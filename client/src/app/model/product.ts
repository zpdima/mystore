export class Product {
  id?: number;
  name?: string;
  description?: string;
  remain?: number;
  price?: number;
  categoryId?: number;
  categoryName?: string;
  testField?: BigInteger;
}
