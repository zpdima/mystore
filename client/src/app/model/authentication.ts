interface IAuthority {
  authority: string;
}

// export interface IAuthorities{
//   authority?: IAuthority[];
// }

interface IDetails {
  remoteAddress?: string;
  sessionId?: string;
}

interface IPrincipal {
  password?: string;
  username?: string;
  authorities?: IAuthority[];
  accountNonExpired?: boolean;
  accountNonLocked?: boolean;
  credentialsNonExpired?: boolean;
  enabled?: true;
}

export class Authentication {
  authorities: IAuthority[];
  details: IDetails;
  authenticated: boolean;
  principal: IPrincipal;
  credentials?: string;
  name: string;
}
