import {Injectable} from '@angular/core';
import {HttpClient, HttpResponse} from "@angular/common/http";
import {map} from "rxjs/operators";
import {environment} from "../../environments/environment";
import {Authentication} from "../model/authentication";
import {Observable} from "rxjs";

// const MediaType_JSON = "application/json;charset=UTF-8";
const baseURL = environment.serverURL;


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  USER_NAME_SESSION_ATTRIBUTE_NAME = 'authenticatedUser';

  public username: String;
  public password: String;

  public auth: Authentication;

  constructor(private http: HttpClient) {
    this.getAuthentication().subscribe(resp => {
      // display its headers
      // console.log(resp);
      // console.log(resp.headers);
      const keys = resp.headers.keys();
      let headers = keys.map(key =>
        `${key}: ${resp.headers.get(key)}`);
      this.auth = <Authentication>resp.body;
    });

  }

  authenticationService(username: String, password: String) {
    return this.http.get(`${baseURL}/api/v1/basicauth`,
      {
        headers: {
          Authorization: this.createBasicAuthToken(username, password)
          // , withCredentials: 'true'
          // , 'X-Requested-With': 'XMLHttpRequest'
          // , 'Cookie': 'JSESSIONID=4DD03B99E992A05AC1859B1482E5B04F'
        }
      }).pipe(map((res) => {
      this.username = username;
      this.password = password;
      this.registerSuccessfulLogin(username, password);
      this.auth = <Authentication>res;
      console.log(this.auth);
    }));
  }

  getAuthenticationResponse(username: String, password: String): Observable<HttpResponse<Authentication>> {
    this.username = username;
    this.password = password;
    return this.http.get<Authentication>(
      `${baseURL}/api/v1/basicauth`, {
        headers: {
          Authorization: this.createBasicAuthToken(username, password)
          , withCredentials: 'true'
          , 'X-Requested-With': 'XMLHttpRequest'
        },
        observe: 'response'
      });
  }

  getAuthentication(): Observable<HttpResponse<Authentication>> {
    return this.http.get<Authentication>(
      `${baseURL}/api/v1/info`, {observe: 'response'});
  }


  createBasicAuthToken(username: String, password: String) {
    let token = 'Basic ' + window.btoa(username + ":" + password);
    return token;
  }

  registerSuccessfulLogin(username, password) {
    sessionStorage.setItem(this.USER_NAME_SESSION_ATTRIBUTE_NAME, username)
  }

  logout() {
    sessionStorage.removeItem(this.USER_NAME_SESSION_ATTRIBUTE_NAME);
    this.username = null;
    this.password = null;
    // console.log(` --- logout ${this.auth.name}`);
    return this.http.get(`${baseURL}/api/v1/logout`).subscribe(result => {
        console.log(result);
      }, err => {
      }, () => {
        this.getAuthentication().subscribe(resp => {
          // display its headers
          console.log(resp);
          console.log(resp.headers);
          const keys = resp.headers.keys();
          let headers = keys.map(key =>
            `${key}: ${resp.headers.get(key)}`);
          this.auth = <Authentication>resp.body;
        });
      }
    );
  }

  isUserLoggedIn() {
    let user = sessionStorage.getItem(this.USER_NAME_SESSION_ATTRIBUTE_NAME)
    if (user === null) return false
    return true
  }

  getLoggedInUserName() {
    let user = sessionStorage.getItem(this.USER_NAME_SESSION_ATTRIBUTE_NAME)
    if (user === null) return ''
    return user
  }

  hasRole(authority: string): boolean {
    if (this.auth) {
      // this.auth.authorities.forEach(a => {
      //   console.log(` --- authorities ${a.authority}`);
      // });
      return this.auth.authorities.find(a => a.authority == authority) != null;
    }

    return false;
  }

  // getUserName(){
  //   return this.auth.name || 'Anonimous';
  // }


}
