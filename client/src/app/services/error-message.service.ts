import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ErrorMessageService {


  showError: boolean = false;
  textError: string;

  constructor() { }
}
