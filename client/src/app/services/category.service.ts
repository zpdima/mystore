import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from "@angular/common/http";
import {AuthService} from "./auth.service";
import {environment} from "../../environments/environment";
import {Observable} from "rxjs";
import {ICategory} from "../model/category";

const baseURL = environment.serverURL;


@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  constructor(private http: HttpClient, private auth: AuthService) {
  //console.log(` --- constructor CategoryService`);
  }

  public getCategories(): Observable<any> {
    let headers = new HttpHeaders();

    const params = new HttpParams();
    const options = {
      params: params,
      headers: headers
    };

    return this.http.get(baseURL + "/api/adm/category", options);
  }

  public getCategoriesForAnonymous(): Observable<any> {
    let headers = new HttpHeaders();

    const params = new HttpParams();
    const options = {
      params: params,
      headers: headers
    };

    return this.http.get(baseURL + "/api/category", options);
  }


  public deleteById(id): Observable<any>{

    let headers = new HttpHeaders();

    const params = new HttpParams();
    // // .append("userId", "32");
    // //   .append("userId", String(this.auth.getCurrentUserId()));

    // const body = {"id": id};

    const options = {
      params: params,
      // body: body,
      headers: headers
    };


    return this.http.delete(baseURL + "/api/adm/category/" + id, options);
  };

  public saveCategory(category: ICategory): Observable<any> {

    let headers = new HttpHeaders();
    const options = {
      // params: params,
      headers: headers
    };

    if (category.id) {
    //console.log(` --- PUT category`);
      return this.http.put(baseURL + "/api/adm/category", category, options);
    } else {
    //console.log(` --- POST category`);
      return this.http.post(baseURL + "/api/adm/category", category, options);
    }
  }

}
