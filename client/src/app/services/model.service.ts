import {Injectable, Input, SimpleChange} from '@angular/core';
import {Category} from "../model/category";
import {Product} from "../model/product";
import {CategoryService} from "./category.service";
import {ProductService} from "./product.service";
import {Router} from "@angular/router";

@Injectable({
  providedIn: 'root'
})
export class ModelService {

  // showError: boolean = false;
  // textError: string;
  // jsonError: ErrorJson;


  public categoriesPerPage = 10;
  public selectedCategoryPage = 1;
  public categories: Category[] = new Array<Category>();
  private locator = (c: Category, id: number) => c.id == id;
  currentCategoryId: number = 1;

  public productPerPage = 3;
  public selectedProductPage = 1;
  private products: Product[] = new Array<Product>();
  public countProduct: number;

  constructor(private categoryService: CategoryService,
              private productService: ProductService,
              private router: Router) {
    this.categoryService.getCategoriesForAnonymous().subscribe(data => {
        this.categories = data
      }, err => {
        console.error(err);
        return err;
      },
      () => {
        //console.log("Ok");
        this.productService.getCountProducts(this.currentCategoryId).subscribe(data => {
          this.countProduct = data;
        });
        this.productService.getProductCategoryPage(this.currentCategoryId, this.selectedProductPage - 1, this.productPerPage).subscribe(data => {
          this.products = data;
        }, err => {
          console.error(err);
          return err;
        }, () => {
          console.log("Ok");
        })
      });
  }

  refreshProducts() {
    // console.log(` --- model refreshProducts`);
    this.productService.getCountProducts(this.currentCategoryId).subscribe(data => {
      this.countProduct = data;
    });
    this.productService.getProductCategoryPage(this.currentCategoryId, this.selectedProductPage - 1, this.productPerPage).subscribe(data => {
      this.products = data;
    }, err => {
      console.error(err);
      return err;
    }, () => {
    })
  }

  ngOnChanges(changes: { [property: string]: SimpleChange }) {
    console.log(`ngOnChanges`);
    console.log(changes);
    // let change = changes["bgClass"];
    // if (!change.isFirstChange() && classList.contains(change.previousValue)) {
    //   classList.remove(change.previousValue);
    // }
    // if (!classList.contains(change.currentValue)) {
    //   classList.add(change.currentValue);
    // }
  }

  ngDoCheck(changes: { [property: string]: SimpleChange }) {
    console.log(` --- !!! ngDoCheck !!!`);
  }

  getCategories(): Category[] {
    return this.categories;
  }

  getProducts(): Product[] {
    return this.products;
  }

  getProductCategoryPage(categoryId: number, page: number, size: number, search?: string) {
    console.log(` --- find ${search}`);
    if (search) {
      console.log(` --- find ${search}`);
      this.productService.getCountProducts(categoryId, search).subscribe(data => {
          this.countProduct = data;
        }, err => {
          console.error(err);
          return err;

        }, () => {
          console.log(` --- count product ${this.countProduct}`)
        }
      );
      this.productService.getProductCategoryPage(categoryId, page, size, search).subscribe(data => {
          this.products = data
        }, err => {
          console.error(err);
          return err;
        },
        () => {
          //console.log("Ok");
        }
      );


    } else {
      console.log(` --- all `);
      this.productService.getCountProducts(categoryId).subscribe(data => {
          this.countProduct = data;
        }, err => {
          console.error(err);
          return err;

        }, () => {
          console.log(` --- count product ${this.countProduct}`)
        }
      );
      this.productService.getProductCategoryPage(categoryId, page, size).subscribe(data => {
          this.products = data
        }, err => {
          console.error(err);
          return err;
        },
        () => {
          //console.log("Ok");
        }
      );

    }
  }


}
