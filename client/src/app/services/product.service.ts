import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from "@angular/common/http";
import {AuthService} from "./auth.service";
import {Observable} from "rxjs";
import {environment} from "../../environments/environment";
import {Product} from "../model/product";

const baseURL = environment.serverURL;

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(private http: HttpClient, private auth: AuthService) {
  }


  public getProductCategoryPage(categoryId: number, page: number, size: number, search?: string): Observable<any> {

    let headers = new HttpHeaders();
    let params;
    if (search && search != "") {
      params = new HttpParams().set('category_id', String(categoryId))
        .append("page", String(page))
        .append("size", String(size))
        .append("search", search);
    } else {
      params = new HttpParams().set('category_id', String(categoryId))
        .append("page", String(page))
        .append("size", String(size))
    }


    const options = {
      params: params,
      headers: headers
    };

    return this.http.get(baseURL + "/api/product", options);
  }

  public getCountProducts(categoryId: number, search?: string): Observable<any> {

    console.log(` --- search ${search}`);

    let headers = new HttpHeaders();

    let params;
    if (search && search != "") {
      params = new HttpParams().set('category_id', String(categoryId)).append("search", search);
    } else {
      params = new HttpParams().set('category_id', String(categoryId));
    }
    console.log(params);

    const options = {
      params: params,
      headers: headers
    };

    return this.http.get(baseURL + "/api/product-count-category", options);
  }

  public deleteById(id): Observable<any>{
    console.log(` --- delete id=${id}`);
    return this.http.delete(baseURL + "/api/adm/product/" + id);
  };

  public saveProduct(product: Product): Observable<any> {

    // let headers = new HttpHeaders();
    // const options = {
    //   // params: params,
    //   headers: headers
    // };

    if (product.id) {
      //console.log(` --- PUT product`);
      return this.http.put(baseURL + "/api/adm/product", product/*, options*/);
    } else {
      //console.log(` --- POST product`);
      return this.http.post(baseURL + "/api/adm/product", product/*, options*/);
    }
  }


}
