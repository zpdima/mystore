package cf.zpdima.store.filter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;
import java.util.Random;
import java.util.stream.Collectors;

@Component
@WebFilter(urlPatterns = {"/*"})
public class LogFilter implements Filter {
    private static final Logger logger = LoggerFactory.getLogger(LogFilter.class);

    //    private FilterConfig config = null;
    private boolean active = true;




    @Override
    public void destroy() {
        // TODO Auto-generated method stub
//        config = null;
        logger.info("--- destroy " + LogFilter.class);

    }


    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {
        // TODO Auto-generated method stub
//		loggerger.info(" --- doFilter " + servletRequest.getParameterMap());
        if (active) {
            HttpServletRequest httpRequest = (HttpServletRequest) servletRequest;
            HttpServletResponse httpResponse = (HttpServletResponse) servletResponse;

            logger.info("--- doFilter remote addr = " + servletRequest.getRemoteAddr() + " user : " + httpRequest.getRemoteUser() + " path : " + httpRequest.getServletPath() + " method: " + httpRequest.getMethod() + " CharacterEncoding = "
                    + servletRequest.getCharacterEncoding() + " locale: " + servletRequest.getLocale());
            Map<String, String[]> map = servletRequest.getParameterMap();
            for (String key : map.keySet()) {
                String value = "";
                for (String c : map.get(key)) {
                    value += c;
                }
                logger.info(" ---  Key : " + key + " Value : " + value);
            }
//            if ("POST".equalsIgnoreCase(httpRequest.getMethod())) {
//                String body = httpRequest.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
//                logger.info(" --- body : '{}'", body);
//            }
        }
        filterChain.doFilter(servletRequest, servletResponse);
    }


    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        // TODO Auto-generated method stub
        logger.info("--- init logFilter dir : {}", System.getProperty("user.dir"));
//        for (int i = 1 ; i<=20; i++) {
//            logger.info("INSERT INTO `store`.`categories`(`id`, `name`, `description`, `status`) VALUES ({}, 'Category{}', 'Category {}', 1);", i, i ,i);
//
//            for(int j = 1; j <= 217 ; j++ ){
//                Random r = new Random();
//                float random = (10f + r.nextFloat() * (10000f - 10f));
//                float price = (float) (Math.round(random * Math.pow(10, 2)) / Math.pow(10, 2));
//                Random r1 = new Random();
//                int remain = r.nextInt((1000 - 0) + 1) + 0;
//                logger.info("INSERT INTO `store`.`products`( `category_id`, `name`, `price`, `description` , `remain`) VALUES ( {}, 'Product {} ', {}, 'Product{} {}', {});", i ,j , price, i,  i*1000+j, remain);
//            }
//
//        }
    }

}
