package cf.zpdima.store.service.impl;

import cf.zpdima.store.model.Category;
import cf.zpdima.store.model.Product;
import cf.zpdima.store.repository.CategoryRepository;
import cf.zpdima.store.repository.ProductRepository;
import cf.zpdima.store.service.ProductService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class ProductServiceImpl implements ProductService {

    private static Logger logger =
            LoggerFactory.getLogger(ProductServiceImpl.class);


    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private ProductRepository productRepository;


    @Override
    @Transactional(readOnly = true)
    public Product findById(Long id) {
        return productRepository.getOne(id);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Product> findAll() {
        return productRepository.findAll();
    }

    @Override
    public Product save(Product product) {
        return productRepository.save(product);
    }

    @Override
    public void delete(Long id) {
        productRepository.deleteById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Product> findBycAndCategoryIdOrderById(Long categoryId, Integer page, Integer size) {
        Pageable pageable = PageRequest.of(page, size, Sort.by("id"));
        Category category = categoryRepository.getOne(categoryId);
        return productRepository.findByCategory(category, pageable);
    }


    @Override
    @Transactional(readOnly = true)
    public Long countByCategoryId(Long categoryId) {
        Category category = categoryRepository.getOne(categoryId);
        return productRepository.countByCategory(category);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Product> findBycAndCategoryAndLikeNameIdOrderById(Long categoryId,
                                                                  String search,
                                                                  Integer page,
                                                                  Integer size) {
        Pageable pageable = PageRequest.of(page, size, Sort.by("id"));
        Category category = categoryRepository.getOne(categoryId);
        return productRepository.findByCategoryAndNameIgnoreCaseContaining(category, search, pageable);
    }


    @Override
    @Transactional(readOnly = true)
    public Long countByCategoryIdAndLikeName(Long categoryId, String search) {
        Category category = categoryRepository.getOne(categoryId);
        return productRepository.countByCategoryAndNameIgnoreCaseContaining(category, search);
    }

}
