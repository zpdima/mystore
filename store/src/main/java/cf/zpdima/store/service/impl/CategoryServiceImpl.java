package cf.zpdima.store.service.impl;

import cf.zpdima.store.model.Category;
import cf.zpdima.store.repository.CategoryRepository;
import cf.zpdima.store.service.CategoryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;

@Service
@Transactional
public class CategoryServiceImpl implements CategoryService {

    private static Logger logger =
            LoggerFactory.getLogger(CategoryServiceImpl.class);


    @PersistenceContext
    private EntityManager em;

    @Autowired
    private CategoryRepository categoryRepository;


    @Autowired
    private Environment environment;

    @PostConstruct
    public void init() {
        logger.info(" --- PostConstructor {CategoryServiceImpl}");

        for (final String profileName : environment.getActiveProfiles()) {
            logger.info(" --- --- Currently active profile - {}", profileName);
        }
    }

    @Transactional(readOnly = true)
    @Override
    public Category findById(Long id){
        return categoryRepository.getOne(id);
    };

    @Transactional(readOnly = true)
    @Override
    public List<Category> findAllRep() {
        List<Category> listReturn = categoryRepository.findAll();
        return listReturn;
    }

    @Transactional(readOnly = true)
    @Override
    public List<Category> findEmAll() {
        logger.info(" --- findAll()");
        TypedQuery<Category> query = em.createQuery("select c from Category c", Category.class);
//        query. setParameter ( "dbeg", dbeg) ;
//        query. setParameter ( "dend", dend) ;
        List<Category> listReturn = query.getResultList();
        return listReturn;

    }


    @Transactional(readOnly = true)
    @Override
    public List<Category> findFullAll() {
        List<Category> listReturn = categoryRepository.findFullAll();
        return listReturn;
    }

    @Transactional(readOnly = true)
    @Override
    public List<Category> findShortAll() {
        List<Category> listReturn = categoryRepository.findShortAll();
        return listReturn;
    }


    @Transactional(readOnly = true)
    @Override
    public List<Category> findAll() {
        List<Category> listReturn = categoryRepository.findAll();
        return listReturn;
    }


    @Override
    public Category save(Category category){
        return categoryRepository.save(category);
    }

    @Override
    public void delete(Long id){
        categoryRepository.deleteById(id);
    }


}
