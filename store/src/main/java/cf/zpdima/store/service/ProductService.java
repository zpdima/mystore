package cf.zpdima.store.service;

import cf.zpdima.store.model.Product;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface ProductService {

    Product findById(Long id);

    public List<Product> findAll();

    Product save(Product product);

    void delete(Long id);

    List<Product> findBycAndCategoryIdOrderById(Long categoryId, Integer page, Integer size);

    Long countByCategoryId(Long categoryId);

    List<Product> findBycAndCategoryAndLikeNameIdOrderById(Long categoryId,
                                                           String search,
                                                           Integer page,
                                                           Integer size);

    Long countByCategoryIdAndLikeName(Long categoryId, String search);
}
