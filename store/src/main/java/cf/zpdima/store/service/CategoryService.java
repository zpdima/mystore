package cf.zpdima.store.service;

import cf.zpdima.store.model.Category;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface CategoryService {


    Category findById(Long id);

    List<Category> findShortAll();

    public List<Category> findAll();

    public List<Category> findAllRep();

    List<Category> findEmAll();

    List<Category> findFullAll();

    Category save(Category category);

    void delete(Long id);
}
