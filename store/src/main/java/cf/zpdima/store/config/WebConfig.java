package cf.zpdima.store.config;

import cf.zpdima.store.converters.JsonToProductConveret;
import cf.zpdima.store.converters.ProductToJsonConverter;
import cf.zpdima.store.service.CategoryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.format.FormatterRegistry;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
//@EnableWebMvc
public class WebConfig implements WebMvcConfigurer {

    private final Logger logger = LoggerFactory.getLogger(WebConfig.class);

//    @Autowired
//    CategoryService categoryService;

    @Autowired
    JsonToProductConveret jsonToProductConveret;


//    @Override
//    public void addCorsMappings(CorsRegistry registry) {
//        registry.addMapping("/**").allowedOrigins("*");
//
//    }


    @Override
    public void addFormatters(FormatterRegistry registry) {

//        logger.info(" --- jsonToProductConveret {}", jsonToProductConveret);
        logger.info(" --- addConverters");
        registry.addConverter(jsonToProductConveret);
        registry.addConverter(new ProductToJsonConverter());

//        logger.info(" --- addFormatters(cardPatientStatusFormatter)");
//        logger.info(" --- i18nRepository {}", i18nRepository);
//        CardPatientStatusFormatter cardPatientStatusFormatter = new CardPatientStatusFormatter();
//        cardPatientStatusFormatter.setI18nRepository(i18nRepository);
//        registry.addFormatter(cardPatientStatusFormatter);
    }




}
