package cf.zpdima.store.repository;

import cf.zpdima.store.model.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface CategoryRepository extends JpaRepository<Category, Long> {
//    @Query(
//            value = "SELECT * FROM categories c LEFT JOIN products p ON c.id = p.category_id",
//            nativeQuery = true)
//    List<Category> findFullAll();

    @Query(value = "SELECT distinct c FROM Category c LEFT JOIN fetch c.products")
    List<Category> findFullAll();

    @Query(value = "SELECT c FROM Category c ")
    List<Category> findShortAll();

}
