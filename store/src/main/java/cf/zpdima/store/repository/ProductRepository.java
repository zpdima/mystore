package cf.zpdima.store.repository;

import cf.zpdima.store.model.Category;
import cf.zpdima.store.model.Product;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ProductRepository extends JpaRepository<Product, Long> {

    List<Product> findByCategory(Category category, Pageable pageable);

    Long countByCategory(Category category);

    List<Product> findByCategoryAndNameIgnoreCaseContaining(Category category, String search, Pageable pageable);

    Long countByCategoryAndNameIgnoreCaseContaining(Category category, String search);

}
