package cf.zpdima.store.controller;

import cf.zpdima.store.model.Category;
import cf.zpdima.store.model.dto.View;
import cf.zpdima.store.service.CategoryService;
import com.fasterxml.jackson.annotation.JsonView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.*;
import java.util.stream.Collector;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/api")
public class CategoryController {

    private final Logger logger = LoggerFactory.getLogger(CategoryController.class);

    @Autowired
    CategoryService categoryService;



    @JsonView(View.CategoryUI.class)
//    @CrossOrigin(origins = {"http://localhost:4200", "http://127.0.0.1:4200"}, allowCredentials = "true")
    @RequestMapping(value = "/category", method = RequestMethod.GET, produces =  {MediaType.APPLICATION_JSON_VALUE})
    public List<Category> listUI(){
//        List<Category> l =  categoryService.findAll().stream().map(c -> {
//            c.setProducts(null);
//            return c;
//        }).collect(Collectors.toList());
//        List<Category> l =  categoryService.findAll();
        List<Category> l =  categoryService.findAll();

        return l;
    }

    @PreAuthorize("hasRole('ADMIN')")
    @RequestMapping(value = "/adm/category", method = RequestMethod.GET, produces =  {MediaType.APPLICATION_JSON_VALUE})
    public List<Category> listAdm(){
        List<Category> l =  categoryService.findAll();
        return l;
    }

    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping(path = "/adm/category", consumes = {MediaType.APPLICATION_JSON_VALUE}, produces =  {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<?> insertCategory(@RequestBody @Valid Category category,
                                           HttpServletRequest request,
                                           HttpServletResponse response,
                                           Locale locale) {
        //code
        logger.info(" --- POST patient : {} locale: {} - {} - {}", category, request.getLocale(), response.getLocale(), locale );
        int r = 0;
        try {

//            try {
//                Thread.sleep(7000);
//            } catch (InterruptedException e) {
//                // TODO Auto-generated catch block
//                e.printStackTrace();
//            }

//            if(1 ==1) {
//                throw new Exception(" !!! test Exception !!!");
//            }

//            Patient patient = conversionService.convert(patientDTO, Patient.class);
            category = categoryService.save(category);
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }

        return ResponseEntity.ok(category);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @PutMapping(path = "/adm/category", consumes = {MediaType.APPLICATION_JSON_VALUE}, produces =  {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<?> updateCategory(@RequestBody @Valid Category category) {
        logger.info(" --- PUT category : {} ", category);
        try {
            category = categoryService.save(category);
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
        return ResponseEntity.ok(category);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @DeleteMapping(value = "/adm/category/{id}", produces =  {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<?> deleteCategory(@PathVariable Long id) {
        logger.info(" --- DELETE category : {}", id);
        Map<String, String> r = new HashMap<>();
        try {
            categoryService.delete(id);
        } catch (Exception e) {
//            r.put("return", "error");
//            r.put("errorText", e.getMessage());
//            r.put("cause", e.getCause().getMessage());
            return ResponseEntity.badRequest().body(e.getMessage());
        }

        r.put("return", "ok");
        return ResponseEntity.ok(r);
    }


}
