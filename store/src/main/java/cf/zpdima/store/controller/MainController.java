package cf.zpdima.store.controller;

import cf.zpdima.store.model.Category;
import cf.zpdima.store.model.Product;
import cf.zpdima.store.model.enums.CategoryStatus;
import cf.zpdima.store.service.CategoryService;
import cf.zpdima.store.service.ProductService;
import org.flywaydb.core.internal.util.StopWatch;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.annotation.PostConstruct;
import java.util.Locale;
import java.util.Random;

@Controller
public class MainController {

    private final Logger logger = LoggerFactory.getLogger(MainController.class);

    @Autowired
    CategoryService categoryService;

    @Autowired
    ProductService productService;


    @PostConstruct
    void init() {
        if(1 == 1 ){
            return;
        }

        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        logger.info("--- !!! stopWatch.start()");
        logger.info("--- init MainController dir : {}", System.getProperty("user.dir"));
        Long num = 0l;
        for (int i = 1; i <= 20; i++) {
//            logger.info("INSERT INTO `store`.`categories`(`id`, `name`, `description`, `status`) VALUES ({}, 'Category{}', 'Category {}', 1);", i, i, i);
            Category category = new Category();
            category.setName("Category-" + i);
            category.setDescription("Category numb: " + i);
            category.setStatus(CategoryStatus.OPENED);
            categoryService.save(category);

            for (int j = 1; j <= 50000; j++) {
                Random r = new Random();
                float random = (10f + r.nextFloat() * (10000f - 10f));
                float price = (float) (Math.round(random * Math.pow(10, 2)) / Math.pow(10, 2));
                Random r1 = new Random();
                int remain = r.nextInt((1000 - 0) + 1) + 0;
//                logger.info("INSERT INTO `store`.`products`( `category_id`, `name`, `price`, `description` , `remain`) VALUES ( {}, 'Product {} ', {}, 'Product{} {}', {});", i, j, price, i, i * 1000 + j, remain);
                Product product = new Product();
                product.setName("Product " + j+ ":" + (++num));
                product.setDescription("Product numb: " + j + "-" + i);
                product.setRemain(remain);
                product.setPrice(price);
                product.setCategory(category);
                productService.save(product);
            }

        }
        stopWatch.stop();
        logger.info(" --- End   getData Time : {} ms", stopWatch.getTotalTimeMillis());


    }

    @RequestMapping(value = {"/", "/index"}, method = RequestMethod.GET)
    public String index(Locale locale) {

        logger.info("Welcome home! The client locale is {}.", locale);
        return "index.html";
    }

}
