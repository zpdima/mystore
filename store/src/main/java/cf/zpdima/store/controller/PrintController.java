package cf.zpdima.store.controller;

import cf.zpdima.store.model.Product;
import cf.zpdima.store.model.ReportReplace;
import cf.zpdima.store.model.dto.ProductJSON;
import cf.zpdima.store.service.ProductService;
import com.artofsolving.jodconverter.DocumentConverter;
import com.artofsolving.jodconverter.openoffice.connection.OpenOfficeConnection;
import com.artofsolving.jodconverter.openoffice.connection.SocketOpenOfficeConnection;
import com.artofsolving.jodconverter.openoffice.converter.OpenOfficeDocumentConverter;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.jdom.JDOMException;
import org.jopendocument.dom.template.JavaScriptFileTemplate;
import org.jopendocument.dom.template.TemplateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLConnection;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.*;

import static java.time.LocalDate.now;

@Controller
public class PrintController {

    private final Logger logger = LoggerFactory.getLogger(PrintController.class);

    @Autowired
    ProductService productService;

    @Autowired
    ConversionService conversionService;



    @RequestMapping(value = {"/print-test"}, method = RequestMethod.GET)
    public String printTestDocument(HttpServletRequest request,
                                    @RequestParam(value = "name") String name,
                                    @RequestParam(value = "email") String email,
                                    @RequestParam(value = "manager") String manager,
                                    Locale locale) throws IOException, JDOMException, TemplateException {

        logger.info(" --- /print-test. The client locale is {}.", locale);
        logger.info(" --- {} {} {}", name, email, manager);
        logger.info(" --- {}", request.getServletContext().getRealPath("/docs/template.od1t"));
        logger.info("--- dir : {}", System.getProperty("user.dir"));
        logger.info("--- dir : {}", request.getServletContext().getRealPath("docs/template.odt"));


//        InputStream in = new FileInputStream(request.getServletContext().getRealPath("WEB-INF/classes/static/docs/template.odt"));
        InputStream in = new FileInputStream(System.getProperty("user.dir") + "/docs/template.odt");
        String fileName = "outFile";
//        final java.io.File inputFile = new File(System.getProperty("user.dir")+"/tmp/fileout.odt");
        final java.io.File inputFile = java.io.File.createTempFile(fileName, ".odt");
        logger.info(" --- inputFile file: {}", inputFile);
        try (FileOutputStream out = new FileOutputStream(inputFile)) {
            IOUtils.copy(in, out);
        } catch (IOException e) {
            e.printStackTrace();
        }


        java.io.File outputFile = java.io.File.createTempFile(fileName, ".pdf");
//        File outputFile = new File(
//                System.getProperty("user.dir") + "/out/outfile.pdf");
//        File outputFile = new File(
//                request.getSession().getServletContext().getRealPath("/resources/documents/generated/") +
//                        File.separator +  "fileOut.pdf");


        logger.info(" --- out file: {}", outputFile);
        JavaScriptFileTemplate template = new JavaScriptFileTemplate(inputFile);

        List<ReportReplace> reportReplaceList = new ArrayList<>();
        reportReplaceList.add(new ReportReplace("alpa_company_name", name));
        reportReplaceList.add(new ReportReplace("alpa_email", email));
        reportReplaceList.add(new ReportReplace("general_manager_rus", manager));

        reportReplaceList.forEach(r -> template.setField(r.getPlaceholder(), r.getReplace()));
        template.createDocument().saveAs(inputFile);
//        OpenOfficeConnection connection = new SocketOpenOfficeConnection("192.168.114.46", 8100);
//        OpenOfficeConnection connection = new SocketOpenOfficeConnection("192.168.0.130", 8100);
        OpenOfficeConnection connection = new SocketOpenOfficeConnection(8100);
        connection.connect();
        DocumentConverter converter = new OpenOfficeDocumentConverter(connection);

        if (!outputFile.getParentFile().exists()) {
            outputFile.getParentFile().mkdirs();
        }
        converter.convert(inputFile, outputFile);

        connection.disconnect();


//        return "print.html";
        return "download/file/" + outputFile.getName();
    }


    @RequestMapping(value = "/print-product", method = RequestMethod.GET, produces = "application/pdf")
    public HttpEntity<byte[]> printCardProduct(@RequestParam(value = "id") Long id,
                                               HttpServletResponse response,
                                               HttpServletRequest request) throws IOException, JRException {

        logger.info("--- dir : {}", System.getProperty("user.dir"));
        logger.info("--- dir : {}", request.getServletContext().getRealPath("WEB-INF/classes/jrxml/card_product.jrxml"));

        Product product = productService.findById(id);
        ProductJSON productJSON = conversionService.convert(product, ProductJSON.class);

        Path path = Paths.get("jrxml/card_product.jrxml");
        String pathForPattern;
        if (Files.exists(path)) {
            pathForPattern = "jrxml/card_product.jrxml";
        }else{
            pathForPattern = request.getServletContext().getRealPath("WEB-INF/classes/jrxml/card_product.jrxml");
        }

//        String pathForPattern = "jrxml/card_product.jrxml";
//        String pathForPattern = request.getServletContext().getRealPath("WEB-INF/classes/jrxml/card_product.jrxml");

        ArrayList<Product> dataBeanList = new ArrayList<>();
        dataBeanList.add(product);
        JRBeanCollectionDataSource beanColDataSource = new JRBeanCollectionDataSource(dataBeanList);

        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("DATE", new Date());
        // System.out.println(" --- " + computer);
        File reportPattern = new File(pathForPattern);
        JasperDesign jasperDesign = JRXmlLoader.load(reportPattern);
        JasperReport jasperReport = JasperCompileManager.compileReport(jasperDesign);
        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, beanColDataSource);
//
        File directory = new File("./OUT");
        if (!directory.exists()) {
            directory.mkdir();
            System.out.println("Create dir : " + "./OUT");
        }


        final java.io.File fileTmp = java.io.File.createTempFile("card_product", ".pdf");
        String pathForSaving = fileTmp.getAbsolutePath();
        fileTmp.deleteOnExit();


//        String pathForSaving = "./OUT/card_product" + id + "_"
//                + (new SimpleDateFormat("yy.MM.dd_HH_mm_ss", new Locale("RU"))).format(new Date()) + ".pdf";
        JasperExportManager.exportReportToPdfFile(jasperPrint, pathForSaving);

        // тут 3 метода загрузки файла , исплользовал 1-й метод
        // https://memorynotfound.com/spring-mvc-download-file-examples/

        // File file = new File("E:\\DIMA\\doc\\JAVA\\izuchaem-java-ee-7.pdf");
        File file = new File(pathForSaving);
        if (!file.exists()) {
            throw new FileNotFoundException("file with path: " + pathForSaving + " was not found.");
        }

        byte[] document = FileCopyUtils.copyToByteArray(file);

        HttpHeaders header = new HttpHeaders();
        header.setContentType(new MediaType("application", "pdf"));
        header.set("Content-Disposition", "inline; filename=" + file.getName());
        header.setContentLength(document.length);

        return new HttpEntity<byte[]>(document, header);
    }

}
