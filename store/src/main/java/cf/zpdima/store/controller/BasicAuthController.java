package cf.zpdima.store.controller;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.security.Principal;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api/v1")
public class BasicAuthController {
    private final Logger logger = LoggerFactory.getLogger(BasicAuthController.class);

    @GetMapping(path = "/basicauth")
    public ResponseEntity<Principal> basicauth(final Principal principal) {
        return ResponseEntity.ok(principal);
    }


    @GetMapping
    @RequestMapping("/info")
    public ResponseEntity<Authentication> getInfo(final Principal principal, HttpServletRequest request, HttpServletResponse response) {

//        logger.info(principal.toString());
//
//        Principal principalRequest = request.getUserPrincipal();
//        logger.info(" --- principalRequest = {}", principalRequest.toString());
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        return ResponseEntity.ok(auth);
    }

    @RequestMapping(value="/logout", method = RequestMethod.GET)
    public ResponseEntity<Object> logout (HttpServletRequest request, HttpServletResponse response) {
        logger.info(" --- LogOut !!!");
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        logger.info(" --- LogOut = {} : {}", auth.getCredentials(), auth.getPrincipal().toString());
        Object principal = null;

        if (auth != null){
            new SecurityContextLogoutHandler().logout(request, response, auth);
//            auth = SecurityContextHolder.getContext().getAuthentication();
            principal = auth != null ? auth.getPrincipal() : "anonymousUser";
        }
        return ResponseEntity.ok(principal);
    }



}

