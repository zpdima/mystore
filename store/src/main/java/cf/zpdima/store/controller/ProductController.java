package cf.zpdima.store.controller;

import cf.zpdima.store.model.Product;
import cf.zpdima.store.model.dto.ProductJSON;
import cf.zpdima.store.service.ProductService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

@RestController
@RequestMapping(value = "/api")
public class ProductController {

    private final Logger logger = LoggerFactory.getLogger(ProductController.class);

    @Autowired
    ProductService productService;

    @Autowired
    ConversionService conversionService;


//    @RequestMapping(value = "/product", method = RequestMethod.GET, produces =  {MediaType.APPLICATION_JSON_VALUE})
//    public List<Product> listUI(){
//        List<Product> l =  productService.findAll();
//        return l;
//    }

    @RequestMapping(value = "/product", method = RequestMethod.GET, produces =  {MediaType.APPLICATION_JSON_VALUE})
    public List<Product> list(@RequestParam(value = "category_id", required = true) Long categoryId ,
                              @RequestParam(value = "page", required = true) Integer page,
                              @RequestParam(value = "size", required = true) Integer size,
                              @RequestParam(value = "search", required = false) String search){
        if(search == null) {
            return productService.findBycAndCategoryIdOrderById(categoryId, page, size);
        }else{
            return productService.findBycAndCategoryAndLikeNameIdOrderById(categoryId, search, page, size);
        }
    }

    @RequestMapping(value = "/product-count-category", method = RequestMethod.GET, produces =  {MediaType.APPLICATION_JSON_VALUE})
    public Long list(@RequestParam(value = "category_id", required = true) Long categoryId ,
                     @RequestParam(value = "search", required = false) String search){
        if(search == null) {
            return productService.countByCategoryId(categoryId);
        }else{
            return productService.countByCategoryIdAndLikeName(categoryId, search);
        }

    }

    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping(path = "/adm/product", consumes = {MediaType.APPLICATION_JSON_VALUE}, produces =  {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<?> insertProduct(@RequestBody @Valid ProductJSON productJSON,
                                            HttpServletRequest request,
                                            HttpServletResponse response,
                                            Locale locale) {
        //code
//        logger.info(" --- POST patient : {} locale: {} - {} - {}", productJSON, request.getLocale(), response.getLocale(), locale );
        logger.info(" --- POST product : {}", productJSON);
        int r = 0;
        Product product = null;
        try {
            product = conversionService.convert(productJSON, Product.class);
            logger.info(" --- product : {}", product);
            product = productService.save(product);
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }

        return ResponseEntity.ok(product);
    }


    @PreAuthorize("hasRole('ADMIN')")
    @PutMapping(path = "/adm/product", consumes = {MediaType.APPLICATION_JSON_VALUE}, produces =  {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<?> updateCategory(@RequestBody @Valid ProductJSON productJSON) {
        logger.info(" --- PUT product : {} ", productJSON);
        Product product = null;
        try {
            product = conversionService.convert(productJSON, Product.class);
            logger.info(" --- product : {}", product);
            product = productService.save(product);
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
        return ResponseEntity.ok(product);
    }


    //    @PreAuthorize("hasRole('USER') and #username == authentication.principal.username")
    @PreAuthorize("hasRole('ADMIN')")
    @DeleteMapping(value = "/adm/product/{id}", produces =  {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<?> deleteCategory(@PathVariable Long id) {
        logger.info(" --- DELETE product : {}", id);
        Map<String, String> r = new HashMap<>();
        try {
            productService.delete(id);
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
        r.put("return", "ok");
        return ResponseEntity.ok(r);
    }



}
