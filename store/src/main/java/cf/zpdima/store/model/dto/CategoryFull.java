package cf.zpdima.store.model.dto;

import cf.zpdima.store.model.Product;

import java.util.Set;

public class CategoryFull {
    private Long id;
    private String name;
    private String description;
    private Integer status;
    private Set<Product> products;
}
