package cf.zpdima.store.model;

import cf.zpdima.store.model.dto.View;
import com.fasterxml.jackson.annotation.*;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@NoArgsConstructor
@AllArgsConstructor
//@ToString
@Getter
@Setter
@Entity
@Table(name = "products")
@JsonIgnoreProperties(ignoreUnknown=true)
@JsonInclude(JsonInclude.Include.NON_NULL)
//@JsonIdentityInfo(
//        generator = ObjectIdGenerators.PropertyGenerator.class,
//        property = "id")
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank
    @Size(min=5, max=60)
    @NotNull
    @Pattern(regexp = "^[A-Za-z0-9.:\\- ]+$")
    private String name;

    @NotBlank
    @Size(min=5, max=60)
    @Pattern(regexp = "^[A-Za-z0-9.,-; ]+$")
    private String description;

    private Integer remain;

    private Float price;

    @ManyToOne
    @JoinColumn(name = "category_id")
    @JsonIgnore
    private Category category;

    @Transient
    public Long getCategoryId(){
        return this.category.getId();
    }

    @Transient
    public String getCategoryName(){
        return this.category.getName();
    }

    @Transient
    public Long getTestField() {return 9999999999999999l;};
}
