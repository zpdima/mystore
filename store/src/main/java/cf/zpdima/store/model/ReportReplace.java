package cf.zpdima.store.model;

public class ReportReplace {

    String placeholder;
    Object replace;

    public ReportReplace(String placeholder, Object replace)
    {
        this.placeholder = placeholder;
        this.replace = replace;
    }

    public String getPlaceholder()
    {
        return placeholder;
    }

    public void setPlaceholder(String placeholder)
    {
        this.placeholder = placeholder;
    }

    public Object getReplace() {
        return replace;
    }

    public void setReplace(Object replace) {
        this.replace = replace;
    }
}
