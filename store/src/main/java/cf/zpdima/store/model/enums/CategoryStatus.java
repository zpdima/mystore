package cf.zpdima.store.model.enums;

public enum CategoryStatus {
    CLOSED, OPENED, NEW;
}
