package cf.zpdima.store.model;


import cf.zpdima.store.model.dto.View;
import cf.zpdima.store.model.enums.CategoryStatus;
import com.fasterxml.jackson.annotation.*;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Set;

@NoArgsConstructor
@AllArgsConstructor
@ToString
@Getter
@Setter
@Entity
@Table(name = "categories")
@JsonIgnoreProperties(ignoreUnknown=true)
@JsonInclude(JsonInclude.Include.NON_NULL)
//@JsonIdentityInfo(
//        generator = ObjectIdGenerators.PropertyGenerator.class,
//        property = "id")
public class Category {

    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonView(View.CategoryUI.class)
    private Long id;

    @NotBlank
    @Size(min=5, max=60)
    @NotNull
    @Pattern(regexp = "^[A-Za-z0-9.\\- ]+$")
    @JsonView(View.CategoryUI.class)
    private String name;


    @NotBlank
    @Size(min=5, max=60)
    @Pattern(regexp = "^[A-Za-z0-9.,-; ]+$")
    @JsonView({View.CategoryUI.class, View.Category.class})
    private String description;

    @NotNull
    private CategoryStatus status;

//    @OneToMany(mappedBy = "category", fetch = FetchType.LAZY, cascade=CascadeType.ALL,
//            orphanRemoval=true)
    @OneToMany(mappedBy = "category", fetch = FetchType.LAZY)
    @JsonIgnore
    private Set<Product> products = new HashSet<>();

//    public void addAuthority(String strAuthority){
//        Product authority = new Authority(this.username, strAuthority, this);
//        this.authorities.add(authority);
//    }

    @Transient
    public Long getTestField() {return 9999999999999999l;};
}
