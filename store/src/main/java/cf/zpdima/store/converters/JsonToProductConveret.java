package cf.zpdima.store.converters;

import cf.zpdima.store.model.Category;
import cf.zpdima.store.model.Product;
import cf.zpdima.store.model.dto.ProductJSON;
import cf.zpdima.store.repository.CategoryRepository;
import cf.zpdima.store.service.CategoryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;


@Component
public class JsonToProductConveret implements Converter<ProductJSON, Product> {

    private static Logger logger =
            LoggerFactory.getLogger(JsonToProductConveret.class);

//    @Autowired
//    private CategoryService categoryService;

    @Autowired
    private CategoryRepository categoryRepository;


    @PostConstruct
    void init(){
        logger.info(" --- PostConstruct(JsonToProductConveret)");
    }



    @Override
    public Product convert(ProductJSON productJSON) {
        Product product = new Product();
        product.setId(productJSON.getId());
        product.setName(productJSON.getName());
        product.setDescription(productJSON.getDescription());
        product.setRemain(productJSON.getRemain());
        product.setPrice(productJSON.getPrice());
        Category category;

        logger.info(" ---- categoryRepository {}", categoryRepository);

        category = categoryRepository.getOne(productJSON.getCategoryId());
        product.setCategory(category);
        return product;
    }
}
