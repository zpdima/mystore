package cf.zpdima.store.converters;

import cf.zpdima.store.model.Product;
import cf.zpdima.store.model.dto.ProductJSON;
import org.springframework.core.convert.converter.Converter;

public class ProductToJsonConverter implements Converter<Product, ProductJSON> {

    @Override
    public ProductJSON convert(Product product) {
        ProductJSON productJSON = new ProductJSON();
        productJSON.setId(product.getId());
        productJSON.setName(product.getName());
        productJSON.setDescription(product.getDescription());
        productJSON.setRemain(product.getRemain());
        productJSON.setPrice(product.getPrice());
        productJSON.setCategoryId(product.getCategory().getId());
        return productJSON;

    }

}
