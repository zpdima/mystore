CREATE SEQUENCE IF NOT EXISTS categories_id_seq;
CREATE SEQUENCE IF NOT EXISTS products_id_seq;

CREATE TABLE IF NOT EXISTS categories (
                                     id BIGINT PRIMARY KEY DEFAULT nextval('categories_id_seq'::regclass),
                                     name VARCHAR(256) NOT NULL CONSTRAINT categories_name_uk UNIQUE,
                                     description VARCHAR(256) NOT NULL,
                                     status INT2
);

CREATE TABLE IF NOT EXISTS products (
                                     id BIGINT PRIMARY KEY DEFAULT nextval('products_id_seq'::regclass),
                                     category_id BIGINT,
                                     name VARCHAR(256) NOT NULL,
                                     description VARCHAR(256) NOT NULL,
                                     remain int4,
				     price float8
);


ALTER TABLE "public"."products"
ADD CONSTRAINT "products_category_id" FOREIGN KEY ("category_id") REFERENCES "public"."categories" ("id");
